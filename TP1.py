# -*- coding: utf-8 -*-

import numpy as np;
import matplotlib.pyplot as plt;
import os;
from PIL import Image;
from sklearn.model_selection import train_test_split;
from sklearn.tree import DecisionTreeClassifier;
from sklearn.svm import SVC;
from sklearn.metrics import confusion_matrix;
from sklearn.metrics import accuracy_score;
import matplotlib.image as mpimg;
import cv2;

np.random.seed(42)

random_values = np.random.randint(4, size=1000)
X = random_values
print(X[:10])
print("")

moyenne = round(np.mean(X), 2)
ecart_type = round(np.std(X), 2)
median = round(np.median(X), 2)

print(f"Moyenne : {moyenne}")
print(f"Écart type : {ecart_type}")
print(f"Médiane : {median}")
print("")

X_bis = np.random.uniform(0, 3, size=1000)


moyenne_bis = round(np.mean(X_bis), 2)
ecart_type_bis = round(np.std(X_bis), 2)
median_bis = round(np.median(X_bis), 2)

print(f"Moyenne de X_bis : {moyenne_bis}")
print(f"Écart type de X_bis : {ecart_type_bis}")
print(f"Médiane de X_bis : {median_bis}")

if moyenne == moyenne_bis:
    print("\nLes moyennes de X et X_bis sont égales.")
    print("")
else:
    print("\nLes moyennes de X et X_bis sont différentes.")
    print("")

if ecart_type == ecart_type_bis:
    print("Les écarts types de X et X_bis sont égaux.")
    print("")
else:
    print("Les écarts types de X et X_bis sont différents.")
    print("")

if median == median_bis:
    print("Les médianes de X et X_bis sont égales.")
    print("")
else:
    print("Les médianes de X et X_bis sont différentes.")
    print("")


y = np.sin(X)


amplitude = 0.1
noise = amplitude * np.random.randn(1000)


y_with_noise = y + noise


print(y_with_noise[:10])

plt.figure(figsize=(9,4))
plt.scatter(X, y)
plt.show()

plt.hist(noise, bins=50, alpha=0.7, color='blue', edgecolor='black')

plt.xlabel('Valeurs du bruit gaussien')
plt.ylabel('Fréquence')
plt.title('Histogramme du bruit gaussien')

plt.show()

#C:\Users\robin\OneDrive\Bureau\VisionOrdi\computer_vision_tp1\data1\bike
dossier_images = r'C:\Users\robin\OneDrive\Bureau\VisionOrdi\computer_vision_tp1\data1\bike' 

fichiers = os.listdir(dossier_images)
images = [fichier for fichier in fichiers if fichier.endswith(('.jpg', '.jpeg', '.png', '.gif'))]
nombre_images = len(images)
print("")
print(f"Nombre d'images dans le dossier : {nombre_images}")
print("")

if nombre_images > 0:
    
    chemin_image = os.path.join(dossier_images, images[0])   
    img = Image.open(chemin_image)

    
    format_image = img.format
    taille_image = img.size
    
   

   
    print(f"Format de l'image : {format_image}")
    print(f"Taille de l'image : {taille_image} pixels")
else:
    print("Aucune image trouvée dans le dossier.")
    
    

chemin_image = r'C:\Users\robin\OneDrive\Bureau\VisionOrdi\computer_vision_tp1\data1\bike\Bike1.png'


image_couleur = mpimg.imread(chemin_image)


plt.imshow(image_couleur)
plt.title('Image en couleur')
plt.axis('off')  
plt.show()

image_gris = image_couleur.mean(axis=2)

plt.imshow(image_gris, cmap='gray')
plt.title('Image en noir et blanc')
plt.axis('off')  
plt.show()

#Changement de l'argument origin pour voir la moto à l'envers
plt.imshow(image_couleur[::-1], origin='upper')
plt.title('Image à l\'envers')
plt.axis('off')  
plt.show()

#Dossier de moto et de voiture
bike_folder = r'C:\Users\robin\OneDrive\Bureau\VisionOrdi\computer_vision_tp1\data1\bike'   # Chemin vers le dossier bike
car_folder = r'C:\Users\robin\OneDrive\Bureau\VisionOrdi\computer_vision_tp1\data1\car'     # Chemin vers le dossier car
target_size = (224, 224)  # Taille cible des images

def populate_images_and_labels_lists():
    images = []  # Liste des images
    labels = []  # Liste des labels

    # Liste des classes (dossiers) dans le répertoire
    classes = os.listdir(bike_folder)

    for classe in classes:
        classe_folder = os.path.join(bike_folder, classe)
        if os.path.isdir(classe_folder):
            for filename in os.listdir(classe_folder):
                image_path = os.path.join(classe_folder, filename)
                if os.path.isfile(image_path):
                    # Charger l'image avec OpenCV
                    image = cv2.imread(image_path)
                    print(f'Chemin de limage que je cherche', image_path)

                    # Redimensionner l'image à la taille cible en utilisant l'interpolation bilinéaire
                    image = cv2.resize(image, target_size, interpolation=cv2.INTER_LINEAR)

                    # Ajouter l'image à la liste des images
                    images.append(image)

                    # Ajouter le label correspondant (le nom du dossier) à la liste des labels
                    labels.append(classe)

    return np.array(images), np.array(labels)

images, labels = populate_images_and_labels_lists()

images_np = np.array(images)
labels_np = np.array(labels)

images = np.array([image.flatten() for image in images])

# Diviser les données en ensembles d'entraînement et de test
test_size = 0.2  # Vous pouvez ajuster la proportion de données de test
X_train, X_test, y_train, y_test = train_test_split(images, labels, test_size=0.2, random_state=0)

clf = DecisionTreeClassifier(random_state=0)
clf.fit(X_train, y_train)
clf_svm = SVC(kernel='linear', random_state=0)

clf_svm.fit(X_train, y_train)


y_pred_tree = clf.predict(X_test)  
accuracy_tree = accuracy_score(y_test, y_pred_tree)  


y_pred_svm = clf_svm.predict(X_test) 
accuracy_svm = accuracy_score(y_test, y_pred_svm)  


print("Accuracy du modèle d'arbre de décision : {:.2f}%".format(accuracy_tree * 100))
print("Accuracy du modèle SVM : {:.2f}%".format(accuracy_svm * 100))

# Calculer la matrice 1
confusion_tree = confusion_matrix(y_test, y_pred_tree)
print("Matrice de confusion du modèle d'arbre de décision :")
print(confusion_tree)

# Calculer la matrice 2
confusion_svm = confusion_matrix(y_test, y_pred_svm)
print("Matrice de confusion du modèle SVM :")
print(confusion_svm)


depth = clf.get_depth()
print("Profondeur de l'arbre de décision :", depth)


max_depth_list = list(range(1, 13))


train_accuracy = []
test_accuracy = []


for max_depth in max_depth_list:
    clf = DecisionTreeClassifier(max_depth=max_depth, random_state=0)
    clf.fit(X_train, y_train)
    
    y_pred_train = clf.predict(X_train)
    y_pred_test = clf.predict(X_test)
    
    accuracy_train = accuracy_score(y_train, y_pred_train)
    accuracy_test = accuracy_score(y_test, y_pred_test)
    
    train_accuracy.append(accuracy_train)
    test_accuracy.append(accuracy_test)

plt.plot(max_depth_list, train_accuracy, label='Accuracy d\'entraînement')
plt.plot(max_depth_list, test_accuracy, label='Accuracy de test')
plt.xlabel('max_depth')
plt.ylabel('Accuracy')
plt.title('Accuracy en fonction de max_depth')
plt.legend()
plt.show()


# Chemin vers le dossier "bike2"
bike2_folder = bike_folder = r'C:\Users\robin\OneDrive\Bureau\VisionOrdi\computer_vision_tp1\data1\bike2'   # Chemin vers le dossier bike2

# Charger les images du dossier "bike2" et les aplatir
val_images = []
val_labels = []

for filename in os.listdir(bike2_folder):
    image_path = os.path.join(bike2_folder, filename)
    if os.path.isfile(image_path):
        # Charger l'image avec OpenCV
        image = cv2.imread(image_path)
        # Redimensionner l'image si nécessaire
        image = cv2.resize(image, target_size, interpolation=cv2.INTER_LINEAR)
        # Ajouter l'image aplatie à la liste des images
        val_images.append(image.flatten())
        # Ajouter le label correspondant (le nom du dossier) à la liste des labels
        val_labels.append('bike')  # ou 'car' en fonction du label

# Convertir les listes en tableaux NumPy
val_images = np.array(val_images)
val_labels = np.array(val_labels)

best_max_depth =  15

clf_best = DecisionTreeClassifier(max_depth=best_max_depth, random_state=0)
clf_best.fit(X_train, y_train)
y_pred_val = clf_best.predict(val_images)  

accuracy_val = accuracy_score(val_labels, y_pred_val)  
print("Accuracy des données de validation : {:.2f}%".format(accuracy_val * 100))

def peuplate_and_augment_images_and_labels_lists(image_folder_path):
    images = []  # Liste des images
    labels = []  # Liste des labels

    for filename in os.listdir(bike_folder):
        image_path = os.path.join(bike_folder, filename)
        if os.path.isfile(image_path):
            # Charger l'image avec OpenCV
            image = cv2.imread(image_path)

            # Redimensionner l'image à la taille cible
            image = cv2.resize(image, target_size, interpolation=cv2.INTER_LINEAR)

            # Appliquer la transformation de cropping
            cropped_image = image[48:162, 48:162]

            # Appliquer la transformation en noir et blanc
            grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            grey_image = cv2.cvtColor(grey_image, cv2.COLOR_GRAY2BGR)

            # Ajouter les images transformées à la liste des images
            images.append(image.flatten())
            images.append(cropped_image.flatten())
            images.append(grey_image.flatten())

            # Ajouter les labels correspondants (le nom du dossier) à la liste des labels
            labels.append(classe)
            labels.append(classe)
            labels.append(classe)

    return np.array(images), np.array(labels)


















